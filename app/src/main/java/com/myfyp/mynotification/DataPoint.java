package com.myfyp.mynotification;

import android.location.Location;

/**
 * Created by Admin on 19/4/2016.
 */
public class DataPoint {

    final public long time;
    final public Location location;
    final public double yAcceleration;
    final public boolean home;
    //final public float distance;

    public DataPoint(long t, Location l, double y){
        time = t;
        location = l;
        yAcceleration = y;
        if(l.getLongitude() >= 101.132581 && l.getLongitude() <= 101.144983
                && l.getLatitude() >= 4.333793 && l.getLatitude() <= 4.343004)
        {
            home = false;
        }
        else
            home = true;
        //distance = d;
    }

}