package com.myfyp.mynotification;

/**
 * Created by Admin on 18/4/2016.
 */


public class ActivityEvaluator {
    static private double NO_MOVEMENT_LIMIT = 5;
    static private double ORIENTATION_LIMIT = 5;
    static public float MOO2 = 0;

    static public enum PhysicalActivity {
        MOVING, STANDING, SITTING, HOME, CAMPUS, UNKNOWN;
    }

    static public PhysicalActivity determineActivity(DataPoint... data) throws Exception {
        if (data.length % 2 != 0)
            throw new Exception("Invalid array of data. There must be an even number of DataPoint values.");

        int moving = 0, standing = 0, sitting = 0, unknown = 0;

        for (int i = 0; i < data.length; i += 2) {
            if (isMoving(data[i], data[i + 1])) {
                ++moving;
            } else if (isStanding(data[i], data[i + 1])) {
                ++standing;
            } else if (isSitting(data[i], data[i + 1])) {
                ++sitting;
            }
            else {
                ++unknown;
            }
        }

        if (moving >= standing && moving >= sitting && moving >= unknown) {
            return PhysicalActivity.MOVING;
        } else if (standing >= moving && standing >= sitting && standing >= unknown) {
            return PhysicalActivity.STANDING;
        } else if (sitting >= moving && sitting >= standing && sitting >= unknown) {
            return PhysicalActivity.SITTING;
        }
        else {
            return PhysicalActivity.UNKNOWN;
        }
    }

    static public PhysicalActivity determineLocation(DataPoint... data) throws Exception{
        int homeCount = 0, campusCount = 0;
        for(int i = 0; i < data.length; i ++) {
            if (isHome(data[i]))  {
                homeCount ++;
            }else if (isCampus(data[i])) {
                campusCount++;
            }
        }

        if(homeCount >= campusCount)
            return PhysicalActivity.HOME;
        else
            return PhysicalActivity.CAMPUS;
    }

    // Accelerometer y-axis > 5
    // No change in Location
    static public boolean isSitting(DataPoint dataPoint1, DataPoint dataPoint2) {
        double avgAccel = (Math.abs(dataPoint1.yAcceleration) + Math.abs(dataPoint2.yAcceleration)) / 2;
        return dataPoint1.location.distanceTo(dataPoint2.location) <= NO_MOVEMENT_LIMIT && avgAccel <= ORIENTATION_LIMIT;
    }

    // Accelerometer y-axis <=5
    // No change in Location
    static public boolean isStanding(DataPoint dataPoint1, DataPoint dataPoint2) {
        double avgAccel = (Math.abs(dataPoint1.yAcceleration) + Math.abs(dataPoint2.yAcceleration)) / 2;
        return dataPoint1.location.distanceTo(dataPoint2.location) <= NO_MOVEMENT_LIMIT && avgAccel > ORIENTATION_LIMIT;
    }

    // Accelerometer y-axis doesn't matter
    // Location is different
    static public boolean isMoving(DataPoint dataPoint1, DataPoint dataPoint2) {
        //Log.d("DEBUG", dataPoint1.location.distanceTo(dataPoint2.location) + " = Distance");
        return dataPoint1.location.distanceTo(dataPoint2.location) > NO_MOVEMENT_LIMIT;
    }

    // Accelerometer y-axis doesn't matter
    // Location is same
    static public boolean isHome(DataPoint dataPoint1) {
        if (dataPoint1.location.getLatitude() > 101 && dataPoint1.location.getLatitude() < 101.3) ;
            return true;
        //return dataPoint1.location.distanceTo(null)==0;
    }

    // Accelerometer y-axis doesn't matter
    // Another location
    static public boolean isCampus(DataPoint dataPoint1) {
        //Log.d("DEBUG", dataPoint1.location.distanceTo(dataPoint2.location) + " = Distance");
        return dataPoint1.location.distanceTo(null) == 0;
    }

}

