package com.myfyp.mynotification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.lang.reflect.Method;

/**
 * Created by Admin on 21/4/2016.
 */
public class CallManager extends BroadcastReceiver {
    static boolean flag = false;
    @Override
    public void onReceive(Context context, Intent intent) {
        final TelephonyManager telephone = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        telephone.listen(new PhoneStateListener(){
            @Override
        public void onCallStateChanged(int state, String incomingNumber){
                super.onCallStateChanged(state, incomingNumber);
                if (flag) {
                    if (state == TelephonyManager.CALL_STATE_RINGING) {
                        try {
                            //Log.d("INSIDE BLOCK", " " + flag);
                            Method m = telephone.getClass().getDeclaredMethod("getITelephony");
                            m.setAccessible(true);
                            Object iTelephony = m.invoke(telephone);
                            Method m2 = iTelephony.getClass().getDeclaredMethod("silenceRinger");
                            Method m3 = iTelephony.getClass().getDeclaredMethod("endCall");

                            m2.invoke(iTelephony);
                            m3.invoke(iTelephony);

                            SmsManager smsmanager =  SmsManager.getDefault();
                            smsmanager.sendTextMessage(incomingNumber, null, "I'm busy, call me back later", null, null);

                            //Log.d("FINISH BLOCK", " " + flag);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
           }
        }, PhoneStateListener.LISTEN_CALL_STATE);
    }

    public void setPa(boolean tmp) {
        flag = tmp;
    }
}
