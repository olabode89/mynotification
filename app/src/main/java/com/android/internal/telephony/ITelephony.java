package com.android.internal.telephony;

/**
 * Created by Admin on 21/4/2016.
 */
public interface ITelephony {
    boolean endCall();
    void answerRingingCall();
    void silenceRinger();
}
